import Data.List

data Term = Var String
		   | Abstr String Term
		   | Appl Term Term
	deriving (Show)

data DBTerm = DBVar Int
		   | DBAbstr DBTerm
		   | DBAppl DBTerm DBTerm
    deriving (Show, Eq)

free :: Term -> [String]
free expression =
  helper [] expression
  where
    helper xs (Var x) =
      if elem x xs
      then []
      else [x]
    helper xs (Abstr x expression) =
      helper (x : xs) expression
    helper xs (Appl expr1 expr2) =
      (helper xs expr1) ++ (helper xs expr2)
      
debruijn :: Term -> DBTerm
debruijn expression =
  helper 0 [] expression
  where
    fv = free expression 
    helper d xs (Var x) =
      case elemIndex x xs of
        Just k -> (DBVar k)
        Nothing -> 
          case elemIndex x fv of
          	Just k -> (DBVar (k + d))
    helper d xs (Abstr x expression) =
	  DBAbstr (helper (d+1) (x : xs) expression)
    helper d xs (Appl expr1 expr2) =
      DBAppl (helper d xs expr1) (helper d xs expr2)


alphabet = ['a'..'z'] 

normal :: DBTerm -> Term
normal expression = helper 0 expression
  where
    helper d (DBVar k) =
      if k < d
      then Var $ (alphabet !! (d - k - 1)) : []
      else Var $ (alphabet !! k) : [] 
      								
    helper d (DBAbstr expression) =
      Abstr ((alphabet !! d) : []) (helper (d+1) expression)
    helper d (DBAppl expr1 expr2) =
      Appl (helper d expr1) (helper d expr2)

	  
shift :: Int -> DBTerm -> DBTerm
shift d expression =
  helper 0 expression
  where 
  	helper :: Int -> DBTerm -> DBTerm
  	helper c (DBVar k) =
	  if k < c 
	  then (DBVar k)
	  else (DBVar (k + d))
	helper c (DBAbstr expression) =
	  DBAbstr $ helper (c + 1) expression
	helper c (DBAppl expr1 expr2) =
	  DBAppl (helper c expr1) (helper c expr2)
	  
subst :: DBTerm -> DBTerm -> DBTerm
subst value expression =
  helper 0 expression
  where
    helper :: Int -> DBTerm -> DBTerm
    helper c expression@(DBVar m) =
      if m == c
      then shift c value
      else expression
    helper c (DBAbstr expression) =
      DBAbstr $ helper (c+1) expression
    helper c (DBAppl expr1 expr2) =
      DBAppl (helper c expr1) (helper c expr2)

dbeval :: DBTerm -> DBTerm
dbeval expression@(DBVar _) = expression
dbeval (DBAbstr expression) = DBAbstr (dbeval expression)
dbeval (DBAppl expr1 expr2) =
  apply (dbeval expr1) (dbeval expr2)
  where
  	apply :: DBTerm -> DBTerm -> DBTerm
  	apply (DBAbstr expr1) expr2 =
  	  dbeval $ shift (-1) $ subst (shift 1 expr2) expr1
  	apply expr1 expr2 =
  	  DBAppl expr1 expr2

