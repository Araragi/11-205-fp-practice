import Data.List

data DBTerm = DBVar Int
           | DBAbstraction String Type DBTerm
           | DBApplication DBTerm DBTerm
           | Tru
           | Fls
           | IfThenElse DBTerm DBTerm DBTerm

data Type = TBool
          | TArr Type Type
          | TErr String
    deriving (Show, Eq)

type Env = [(String, Type)]


typeof :: Env -> DBTerm -> Type

typeof env (DBVar k) =
	if k < length env
	then snd $ env !! k
	else TErr "Variable is not in context"
typeof env (DBAbstraction x t1 e) =
	case (typeof ((x, t1): env) e) of
		TErr err -> TErr $ "Error in function body: " ++ err
		t2 -> TArr t1 t2
typeof env (DBApplication e1 e2) =
	case typeof env e1 of
		TArr t1 t2 ->
			let t = (typeof env e2) in
				if t1 == t
				then t2
				else TErr "Incompatible types in application"
		otherwise -> TErr "Arrow type expected"
typeof _ Tru = TBool
typeof _ Fls = TBool
typeof env (IfThenElse cond ifbranch elsebranch) =
	if (typeof env cond) == TBool
	then let t = (typeof env ifbranch) in
		if t == (typeof env elsebranch)
		then t
		else TErr "Incompatible types in if-else branches"
	else TErr "Conditional is not boolean"
	
expr1 = Tru
expr2 = Fls
expr3 = DBAbstraction "x" TBool (IfThenElse (DBVar 0) Tru Fls)
expr4 = DBAbstraction "x" TBool (DBApplication (DBVar 0) (DBVar 0))

main = do
	print $ typeof [] expr1
	print $ typeof [] expr2
	print $ typeof [] expr3
	print $ typeof [] expr4